import React, { useState } from 'react';
import './NewExpense.css';
import ExpenseForm from './ExpenseForm';
const NewExpense = (props) => {
  const [showForm, setShowForm] =  useState(false);
  const addNewExpenseData = (newExpenseData) => {
    const newExpense = {
      ...newExpenseData,
      id: Math.random().toString(),
    };
    props.onAddNewExpense(newExpense);
    setShowForm(false);
  };

  const hideButton = () => {
    setShowForm(true)
  }

  const showButton = () => {
    setShowForm(false);
  }
  return (
    <div className="new-expense">
      {!showForm && <button onClick={hideButton}>Add New Expense</button>}
      {showForm && <ExpenseForm onSubmit={addNewExpenseData} onCancel={showButton} />}
    </div>
  );
};

export default NewExpense;

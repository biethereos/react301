import React, { useState } from 'react';
import Card from '../UI/Card';
import './Expenses.css';
import ExpensesFilter from './ExpensesFilter';
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';
const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState('2022');

  const handleFilterChange = (selectedYear) => {
    setFilteredYear(selectedYear);
  };

  const filteredExpenses = props.expenses.filter(
    (expense) => expense.date.getFullYear().toString() === filteredYear
  );

  
  return (
    <Card className="expenses">
      <ExpensesFilter
        onSelectedYear={handleFilterChange}
        selected={filteredYear}
      />
      <ExpensesChart expenses={filteredExpenses} />
     <ExpensesList items={filteredExpenses} />
    </Card>
  );
};

export default Expenses;
